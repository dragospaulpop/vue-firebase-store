// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import VueMaterial from 'vue-material'
Vue.use(VueMaterial)

import {store, firebaseApp} from './store'
import router from './router'

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.currentUser) {
      next(false)
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresAdmin)) {
    if (!store.getters.currentUser || !store.getters.currentUser.admin) {
      next(false)
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresAnonymous)) {
    if (store.getters.currentUser) {
      next(false)
    } else {
      next()
    }
  } else {
    next()
  }
})

var app = null

firebaseApp.auth().onAuthStateChanged(user => {
  if (!app) {
  /* eslint-disable no-new */
    app = new Vue({
      el: '#app',
      router,
      render: h => h(require('./App')),
      store: store,
      firebase: {
        products: firebaseApp.database().ref('products')
      }
    })
  }
  if (user === null) {
    router.push({name: 'Index'})
  } else {
    store.dispatch('loadUser', {user, app})
  }
  store.commit('user', user)
})
