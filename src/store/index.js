import Vue from 'vue'
import Firebase from 'firebase'
import Vuex from 'vuex'
import VuexFire from 'vuexfire'
import env from '../../env.js'

Vue.use(Vuex)
Vue.use(VuexFire)

export const firebaseApp = Firebase.initializeApp(env)

export const store = new Vuex.Store({
  state: {
    userObj: null,
    user: null,
    errors: {
      email: '',
      password: ''
    },
    products: null
  },
  mutations: {
    ...VuexFire.mutations,
    user (state, user) {
      state.user = user
    },
    setErrors (state, errors) {
      state.errors.email = errors.email || ''
      state.errors.password = errors.password || ''
    }
  },
  actions: {
    changeName ({ commit, state }, name) {
      let uid = state.user.uid
      firebaseApp.database().ref('users/' + uid).child('name').set(name)
    },
    signIn ({ commit, state }, credentials) {
      firebaseApp.auth().signInWithEmailAndPassword(credentials.email, credentials.password).catch(error => {
        let errorCode = error.code
        let errorMessage = error.message
        switch (errorCode) {
          case 'auth/invalid-email':
          case 'auth/email-already-in-use':
          case 'auth/user-disabled':
          case 'auth/user-not-found': {
            let errors = {}
            errors.email = errorMessage
            commit('setErrors', errors)
            break
          }
          case 'auth/weak-password':
          case 'auth/wrong-password': {
            let errors = {}
            errors.password = errorMessage
            commit('setErrors', errors)
            break
          }
        }
      })
    },
    createUser ({ dispatch, commit }, user) {
      firebaseApp.auth().createUserWithEmailAndPassword(user.email, user.password)
        .then(user => {
          dispatch('saveUser', user)
        })
        .catch(error => {
          let errorCode = error.code
          let errorMessage = error.message
          switch (errorCode) {
            case 'auth/invalid-email':
            case 'auth/email-already-in-use':
            case 'auth/user-disabled':
            case 'auth/user-not-found': {
              let errors = {}
              errors.email = errorMessage
              commit('setErrors', errors)
              break
            }
            case 'auth/weak-password':
            case 'auth/wrong-password': {
              let errors = {}
              errors.password = errorMessage
              commit('setErrors', errors)
              break
            }
          }
        })
    },
    saveUser ({ commit, state }, user) {
      firebaseApp.database().ref('users/' + user.uid).set({
        name: 'Anonymous',
        admin: false,
        email: user.email
      })
    },
    saveProduct ({ commit, state }, product) {
      if (product.key) {
        let newProductRef = firebaseApp.database().ref('products/' + product.key)
        delete product.key
        newProductRef.set(product)
      } else {
        let newProductRef = firebaseApp.database().ref('products').push()
        newProductRef.set(product)
      }
    },
    deleteProducts ({ commit, state }, products) {
      let deletes = {}
      products.forEach(key => {
        deletes['products/' + key] = null
      })
      firebaseApp.database().ref().update(deletes)
    },
    loadUser ({ commit, state }, payload) {
      payload.app.$bindAsObject('userObj', firebaseApp.database().ref(`users/${payload.user.uid}`), () => {})
    },
    signout (store, payload) {
      payload.$root.$unbind('userObj')
      firebaseApp.auth().signOut()
    }
  },
  getters: {
    userObj: state => state.userObj,
    products: state => state.products,
    user: state => state.user,
    currentUser: state => state.user && state.userObj ? state.userObj : null,
    errors: state => state.errors
  }
})

// const { bind } = VuexFire.generateBind(store)
// only works with v2 branch

