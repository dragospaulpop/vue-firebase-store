import Vue from 'vue'
import Router from 'vue-router'
import Index from 'components/Index'
import Admin from 'components/Admin'
import LogIn from 'components/AuthForm'
import Account from 'components/Account'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin,
      meta: { requiresAdmin: true }
    },
    {
      path: '/account',
      name: 'Account',
      component: Account,
      meta: { requiresAuth: true }
    },
    {
      path: '/login',
      name: 'LogIn',
      component: LogIn,
      meta: { requiresAnonymous: true }
    }
  ]
})
