export default {
  apiKey: 'safsfsdjksdhgsdhg',
  authDomain: 'my-awesome-database.firebaseapp.com',
  databaseURL: 'https://whatever.firebaseio.com',
  storageBucket: 'blabla.appspot.com',
  messagingSenderId: '234fscv34rt5sdv'
}

/*
Auth rules
Reference: https://firebase.google.com/docs/database/security/

{
  "rules": {
    "users": {
      ".read": "auth != null && root.child('users/'+auth.uid+'/admin').val() === true", // only admins can read a full list of users
      ".write": "auth != null && root.child('users/'+auth.uid+'/admin').val() === true", //only admins can write
      "$uid": {
        ".read": "auth.uid == $uid || root.child('users/'+auth.uid+'/admin').val() === true", // signed in user can read his own data
        ".write": "auth != null && auth.uid === $uid" // signed in user can alter own data
      }
    },
    "products": {
      ".read": "true", //anyone can read product data
      ".write": "auth != null && root.child('users/'+auth.uid+'/admin').val() === true", //only admins can write product data
    }
  }
}

*/