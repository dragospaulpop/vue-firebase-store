# firebase-store

> A vue.js project with Firebase and VueMaterial

# Project Setup

## create firebase project and get config data
https://console.firebase.google.com

## put config data inside env.js
you can just rename the env.example.js file and fill it with your config data

## edit database security rules
go to firebase console -> database -> rules

example rules in env.example.js

## allow email authentication
firebase console -> Authentication -> Sign-in method -> email/password -> enable

## designate an admin user
either create a user throught the register form of the app and go to firebase console -> database -> users -> user id (auto-generated) -> admin -> true

##### or

create a user in the firebase console -> authentication -> users -> add user

copy user UID (ex.: "Shdff..")

go to database -> create "users" node -> inside create node with UID value (ex. "Shdff..") -> add "admin" node -> set to true -> also create "name" node (same level as "admin") and assign it whatever string you want

db should look like this (maybe just import this and edit UID accordingly):
```javascript
{
  "users" : {
    "SpeqhxbsKCat0tOzkMasNIol17J3" : {
      "admin" : false,
      "name" : "test"
    }
  }
}
```

# Project build

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
